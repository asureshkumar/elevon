var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');

    mix.styles([
        "./bower_components/bootstrap-material-design/dist/css/bootstrap-material-design.min.css",
        "./bower_components/bootstrap-material-design/dist/css/ripples.min.css"
    ], './public/css/vendor.css');

    mix.styles([
        "./bower_components/bootstrap-material-design/dist/js/material.min.js",
        "./bower_components/bootstrap-material-design/dist/js/ripples.min.js"
    ], 'public/js/vendor.js');
});
