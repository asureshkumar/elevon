@extends('layouts.app')

@section('content')
<div class='container'>
    <div class='row col-md-12'>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Organization

                </div>
                <div class="panel-body">
                    @if(Auth::user()->id == $model->owner->id)
                    <div class="pull-right">
                        <a href='/organizations/{{$model->id}}/edit' class='pull-left'><i class="fa fa-pencil"></i></a>
<!--                        {!! Form::open(['route'=> ['organizations.delete',$model->id ], 'method'=>'delete']) !!}-->
<!--                        <button role="link" type="submit" class="link"><i class="fa fa-trash text-danger"></i></button>-->
<!--                        {!! Form::close() !!}-->
                    </div>
                    @endif
                    {{ $model->name }}
                </div>
            </div>
        </div>
    </div>
    <div class='row col-md-12'>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Projects
                    @if(Auth::user()->id == $model->owner->id)<a role="link" href="/projects/create" class="pull-right">Create Project</a>@endif
                </div>
                <div class="panel-body">
                    <ul>
                        @foreach($projects as $project)
                        <li><a href="/projects/{{$project->id}}">{{$project->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>
                <div class="panel-body">
                    <ul>
                        @foreach($users as $user)
                        <li>{{$user->name}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection