@extends('layouts.app')

@section('content')
<div class='container'>
    <div class='row'>
        <div class='col-md-10 col-md-offset-1'>
            <a role="button" href="/organizations/create" class="btn btn-primary pull-right">Create Organization</a>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Organization I Own</h4>
                </div>
                <div class="panel-body">
                <table class='table table-bordered table-striped'>
                    <thead>
                        <th>Name</th>
                        <th>Description</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </thead>
                    @foreach($user->organizations as $organization)
                    <tbody>
                    <tr>
                        <td><a href= 'organizations/{{$organization->id}}/'>{{ @$organization->name }}</a></td>
                        <td>{{ @$organization->description }}</td>
                        <td class="text-center"><a href= 'organizations/{{$organization->id}}/edit' class=''><i class="fa fa-pencil"></i></a></td>
                        <td class="text-center">
                            {!! Form::open(['route'=> ['organizations.delete',$organization->id], 'method'=>'delete']) !!}
                            <button role="link" type="submit" class="btn-link"><i class="fa fa-trash text-danger"></i</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    </tbody>
                    @endforeach
                </table>
                </div>
            </div>
            <hr>
            <div class="panel">
                <div class="panel-heading">

                <h4>Organization I Belong to</h4>
                    </div><div class="panel-body">
            <table class='table table-bordered table-striped'>
                <thead>
                <th>Name</th>
                <th>Description</th>
                </thead>
                @foreach($user->belongingOrganizations as $organization)
                @if($organization->user_id != Auth::user()->id)

                <tbody>
                <tr>
                    <td><a href= 'organizations/{{$organization->id}}/'>{{ @$organization->name }}</a></td>
                    <td>{{ @$organization->description }}</td>
                </tr>
                </tbody>
                @endif
                @endforeach
            </table>
                    </div></div>
        </div>
    </div>
</div>

@stop
