@extends('layouts.app')
@section('content')

<div class='container'>
    {{ Form::open(['url' => 'organizations/'.@$organization->id . '/edit', 'method' =>'post', 'class'=>'form',
    'role'=>'form']) }}
    @if($errors && count($errors))
    <div role='alert' class='alert alert-danger'>
        {{ HTML::ul($errors->all()) }}
    </div>
    @endif
    @if(Session::has('success'))
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span>{!! session('success') !!}</div>
    @endif
    <div class='panel col-md-6'>
        <div class='col-md-12'>
            <div class='form-group'>
                <!-- `Name` Field -->
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', @$organization->name, ['class'=>'form-control']) }}
            </div>
            <div class='form-group'>
                <!-- `Description` Field -->
                {{ Form::label('description', 'Description') }}
                {{ Form::text('description', @$organization->description, ['class'=>'form-control']) }}
            </div>
            <div class='col-md-12 text-right'>
                <!-- Form actions -->
                <a href='{{URL::previous()}}' class='btn btn-default'>Cancel</a>
                <button type='submit' class='btn btn-default'>Submit</button>
            </div>
        </div>
    </div>
    {{ Form::close() }}

    <div class='panel col-md-5 col-md-offset-1'>
        <div class='form-group'>
            <!-- `Organization` Field -->
            {!! Form::open(['url' => 'organizations/'.@$organization->id . '/add-user', 'method' =>'post',
            'class'=>'form', 'role'=>'form']) !!}
            {!! Form::select('user_id', $nonUsers, Input::old('user_id'), ['class'=>'form-control', 'placeholder' =>
            'Add a users...']) !!}
        </div>
        <div class='col-md-12 text-right'>
            <!-- Form actions -->
            <button type='submit' class='btn btn-default'>Add</button>
        </div>
        {!! Form::close() !!}
        <table class="table table-bordered table-stripped">
            <thead>
            <th>User Id</th>
            <th>User name</th>
            </thead>
            <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class='container'>
    <div class="panel">
        <div class="panel-heading">
            Projects
            <a role="link" href="/projects/create" class="pull-right">Create Project</a>
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-striped" role="table">
                <thead>
                    <th>Name</th>
                    <th>Total Tasks</th>
                </thead>
                <tbody>
                @foreach($organization->projects as $project)
                <tr>
                    <td><a href="/projects/{{$project->id}}">{{ $project->name}}</a></td>
                    <td>{{count($project->tasks)}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop