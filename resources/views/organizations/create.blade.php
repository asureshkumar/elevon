@extends('layouts.app')

@section('content')
<div class='container'>
    <div class="col-md-6 col-md-offset-3">
        {!! Form::open(array('url' => 'organizations/create', 'method'=> 'post', 'class' => 'form' , 'role' => 'form')) !!}
        @if(@$errors && count($errors))
        <div role='alert' class='alert alert-danger'>
            {!! HTML::ul($errors->all()) !!}
        </div>
        @endif
        <div class='row panel panel-default'>
            <div class="panel-heading">Create a Organization</div>
            <div class="panel-body">
                <div class='col-md-12'>
                    <div class='form-group'>
                        <!-- `Name` Field -->
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', Input::old('name'), ['class'=>'form-control']) !!}
                    </div>
                    <div class='form-group'>
                        <!-- `Description` Field -->
                        {!! Form::label('description', 'Description') !!}
                        {!! Form::textarea('description', Input::old('description'), ['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class='col-md-12 text-right'>
                    <!-- Form actions -->
                    <a href='{!!URL::previous()!!}' class='btn btn-default'>Cancel</a>
                    <button type='submit' class='btn btn-primary'>Submit</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection