<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<div>
    <p>Hi <strong>{{$task->assignedTo->name}}</strong>,</p>
    <div>
        <p>Task {{$task->name}} is completed !</p>
        @if(count($remainingTasks))
        Below are the remaining tasks for you in this project
        <ul>
            @foreach($remainingTasks as $task)
            <li>{{$task->name}}  --  {{ @$task->statusText }} - {{@$task->completion}}% </li>
            @endforeach
        </ul>
        @else
        <p>
            You don't have any remaining tasks in the project : {{$task->project->name}}
        </p>
        @endif
    </div>
</div>
</body>
</html>
