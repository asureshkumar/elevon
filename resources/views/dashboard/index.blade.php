@extends('layouts.app')

@section('content')
<div class="container">
    <div class="">
        <a role="button" class="btn btn-default pull-right" href="/users/{{Auth::user()->id}}/projects/create">Create Project</a>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    <table class='table table-bordered'>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                        </tr>
                        @foreach($organizations as $organization)
                        <tr>
                            <td>{{ @$organization->name }}</td>
                            <td>{{ @$organization->description }}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
