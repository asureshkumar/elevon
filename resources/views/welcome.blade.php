@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome to Elevon - Simple Project Management</div>

                <div class="panel-body">
                    <h3 class="text-center">Elevon simplifies project management for small teams.</h3>
                    <ul class="list-group">
                        <li class="list-group-item">Manage organizations</li>
                        <li class="list-group-item">Create multiple projects under each organization</li>
                        <li class="list-group-item">Assign tasks to users under each project</li>
                        <li class="list-group-item">Handle multiple task statuses</li>
                        <li class="list-group-item">View status of projects</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
