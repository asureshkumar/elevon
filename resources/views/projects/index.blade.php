@extends('layouts.app')

@section('content')
<div class='container'>
    <div class='row'>
        <div class='col-md-12'>
            <a role="link" href="/projects/create" class="btn btn-primary pull-right">Create Project</a>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Projects</h4>
                </div>
                <div class="panel-body">
                    <table class='table table-bordered'>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Organization Name</th>
                            <th>Tasks</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                        @foreach($projects as $project)
                        <tr>
                            <td><a href="/projects/{{@$project->id}}">{{ @$project->name }}</a></td>
                            <td>{{ @$project->description }}</td>
                            <td>{{ @$project->organization->name }}</td>
                            <td><a href= '/projects/{{$project->id}}/tasks'><i class="fa fa-tasks"></i></a></a></td>
                            <td><a href= '/projects/{{$project->id}}/edit'><i class="fa fa-pencil"></i></a></a></td>
                            <td>
                                {!! Form::open(['route'=> ['projects.delete',$project->id], 'method'=>'delete']) !!}
                                <button role="link" type="submit" class="btn-link"><i class="fa fa-trash text-danger"></i</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            {{$projects->links()}}
            </div>
        </div>
    </div>
</div>
@stop
