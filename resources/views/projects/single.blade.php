@extends('layouts.app')

@section('content')
<div class='container'>
    <div class='row col-md-12'>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Projects</div>
                <div class="panel-body">
                    {{ $model->name }}
                    <a class="pull-right" href='/projects/{{$model->id}}/edit'><i class="fa fa-pencil"></i></a>
<!--                        {!! Form::open(['route'=> ['projects.delete',$model->id], 'method'=>'delete']) !!}-->
<!--                        <button role="link" type="submit" class="link pull-right btn-link"><i class="fa fa-trash text-danger"></i></button>-->
<!--                        {!! Form::close() !!}-->
                </div>
            </div>
        </div>
    </div>
    <div class='row col-md-12'>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Tasks
                    <a role="link" href="/projects/{{$model->id}}/tasks/create" class="pull-right">Create Task </a>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <th></th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Assigned To</th>
                        <th></th>
                        </thead>
                        @foreach($model->tasks as $task)
                        <tr>
                            <td>
                                <div class="status-circle-{{$task->status}}"></div>
                            </td>
                            <td>{{$task->name}}</td>
                            <td>{{$task->statusText}}</td>
                            <td>{{$task->assignedTo->name}}</td>
                            <td><a href='/projects/{{$task->project->id}}/tasks/{{$task->id}}/edit'><i class="fa fa-pencil"></i></td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Project Stats</div>
                <div class="panel-body">
                    <table class="table table-bordered table-stripped">
                        <tr>
                            <td>Total Tasks </td>
                            <td>{{ count($model->tasks) }}</td>
                        </tr>
                        <tr>
                            <td><div class="status-circle-1"></div>Completed </td>
                            <td>{{ $model->completed }}</td>
                        </tr>
                        <tr>
                            <td><div class="status-circle-0"></div>Not Completed </td>
                            <td>{{ $model->notCompleted }}</td>
                        </tr><tr>
                            <td><div class="status-circle-2"></div>In Progress</td>
                            <td>{{ $model->inProgress }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection