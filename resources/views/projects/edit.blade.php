@extends('layouts.app')
@section('content')

<div class='container'>
    {{ Form::open(['url' => '/projects/'.@$project->id . '/edit', 'method' =>'post', 'class'=>'form', 'role'=>'form']) }}
    @if($errors && count($errors))
    <div role='alert' class='alert alert-danger'>
        {{ HTML::ul($errors->all()) }}
    </div>
    @endif
    @if(Session::has('success'))
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span>{!! session('success') !!}</div>
    @endif
    <div class='row col-md-8 col-md-offset-2'>
        <div class='col-md-12'>
            <div class="panel panel-default">
            <div class="panel-heading">Edit project</div>
            <div class="panel-body">
            <div class='form-group'>
                <!-- `Name` Field -->
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', @$project->name, ['class'=>'form-control']) }}
            </div>
            <div class='form-group'>
                <!-- `Description` Field -->
                {{ Form::label('description', 'Description') }}
                {{ Form::text('description', @$project->description, ['class'=>'form-control']) }}
            </div>
            <div class='form-group'>
                <!-- `Organization` Field -->
                {!! Form::label('start_date', 'Start Date') !!}
                {!! Form::input('date','start_date', @$project->start_date, ['class'=>'form-control']) !!}
            </div>
            <div class='form-group'>
                <!-- `Organization` Field -->
                {!! Form::label('end_date', 'End Date') !!}
                {!! Form::input('date', 'end_date', @$project->end_date, ['class'=>'form-control']) !!}
            </div>
        <div class='col-md-12 text-right'>
            <!-- Form actions -->
            <a href='{{URL::previous()}}' class='btn btn-default'>Cancel</a>
            <button type='submit' class='btn btn-primary'>Submit</button>
        </div>
    </div>
        </div>

        {{ Form::close() }}
</div>
@stop