@extends('layouts.app')
@section('content')

<div class='container'>
    {{ Form::open(['url' => '/projects/'.@$id . '/tasks/' . @$task->id . '/edit', 'method' =>'post', 'class'=>'form', 'role'=>'form']) }}
    @if($errors && count($errors))
    <div role='alert' class='alert alert-danger'>
        {{ HTML::ul($errors->all()) }}
    </div>
    @endif
    @if(Session::has('success'))
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span>{!! session('success') !!}</div>
    @endif
    <div class='row col-md-8 col-md-offset-2 panel'>
        <div class='col-md-12'>
            <div class='form-group'>
                <!-- `Name` Field -->
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', @$task->name, ['class'=>'form-control']) }}
            </div>
            <div class='form-group'>
                <!-- `Description` Field -->
                {{ Form::label('description', 'Description') }}
                {{ Form::text('description', @$task->description, ['class'=>'form-control']) }}
            </div>
            <div class='form-group'>
                <!-- `Organization` Field -->
                {!! Form::label('assigned_to', 'Assigned To') !!}
                {!! Form::select('assigned_to', $users,   @$task->assigned_to, ['class'=>'form-control', 'placeholder' => 'Pick an assignee...']) !!}
            </div>
            <div class='form-group col-md-6'>
                <!-- `Organization` Field -->
                {!! Form::label('start_date', 'Start Date') !!}
                {!! Form::input('date','start_date', @$task->start_date, ['class'=>'form-control']) !!}
            </div>
            <div class='form-group col-md-6'>
                <!-- `Organization` Field -->
                {!! Form::label('end_date', 'End Date') !!}
                {!! Form::input('date', 'end_date', @$task->end_date, ['class'=>'form-control']) !!}
            </div>
            @if(Auth::user()->id == $task->assigned_to)
            <div class='form-group col-md-6'>
                <!-- `Organization` Field -->
                {!! Form::label('completion', 'Completion') !!}
                {!! Form::input('range', 'completion', @$task->completion, ['id' => 'completion', 'min' => '0',  'max' => '100' ]) !!}
                <div id="completion-value"></div>
            </div>
            <div class='form-group col-md-6'>
                <!-- `Organization` Field -->
                {!! Form::label('status', 'Status') !!} <div id="completion-value"></div>
                {!! Form::select('status', @$task->statusDisplay,   @$task->status, ['class'=>'form-control', 'placeholder' => 'Task status...']) !!}
            </div>
            @endif
        </div>
        <div class='col-md-12 text-right'>
            <!-- Form actions -->
            <a href='{{URL::previous()}}' class='btn btn-default'>Cancel</a>
            <button type='submit' class='btn btn-default'>Submit</button>
        </div>
    </div>
    {{ Form::close() }}
</div>
@stop
