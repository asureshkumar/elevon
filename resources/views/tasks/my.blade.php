@extends('layouts.app')

@section('content')
<div class='container'>
    <div class='row'>
        <div class='col-md-12'>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Tasks</h4>
                </div>
                <div class="panel-body">
                    <table class='table table-bordered'>
                        <tr>
                            <th>Name</th>
                            <th>Project</th>
                            <th>Assigned To</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                        @foreach($tasks as $task)
                        <tr>
                            <td>{{ @$task->name }}</td>
                            <td>{{ @$task->project->name }}</td>
                            <td>{{ @$task->assignedTo->name }}</td>
                            <td>{{ @$task->start_date }}</td>
                            <td>{{ @$task->end_date }}</td>
                            <td><div class="status-circle-{{$task->status}}"></div>{{ @$task->statusText }} - {{@$task->completion}}%</td>
                            <td><a href='/projects/{{$task->project->id}}/tasks/{{$task->id}}/edit'><i class="fa fa-pencil"></i></a>
                            </td>
                            <td>
                                {!! Form::open(['route'=> ['tasks.my.delete', $task->id], 'method'=>'delete']) !!}
                                <button role="link" type="submit" class="btn-link"><i class="fa fa-trash text-danger"></i</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@stop
