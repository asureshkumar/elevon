<?php namespace Elevon\Repositories;


use Elevon\User;
use Illuminate\Support\Facades\Auth;

class UserRepository extends BaseRepository
{

    public function __construct()
    {

        $this->model = Auth::user();;

    }


    public function getLatestFiveOrganizations()
    {

        return $this->model->organizations(function ($query) {

            $query->latest();

        })->take(5)->get();

    }


}
