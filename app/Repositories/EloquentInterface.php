<?php namespace Elevon\repositories;


interface EloquentInterface {

    public function getAll();

    public function getById($id);

    public function create($input);

    public function update($id, $input);

    public function delete($id);

    public function paginate($page, $columns);

}
