<?php namespace Elevon\Repositories;

use Elevon\Project;
use Illuminate\Support\Facades\Auth;

class ProjectRepository extends BaseRepository
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;

    /**
     * @param Project $model
     * @param OrganizationRepository $organizationRepository
     */
    public function __construct(Project $model, OrganizationRepository $organizationRepository)
    {

        $this->model = $model;

        $this->organizationRepository = $organizationRepository;
    }

    public function getAllProjectsForUser()
    {
        return $this->model->with('organization')->whereUserId(Auth::user()->id)->paginate(10);
    }

    public function getProjectWithTasks($id) {

        return $this->model->with(['tasks', 'tasks.assignedTo'])->find($id);

    }

    public function getUsersByProjectId($id) {

        $project = $this->model->find($id);

        $users = $this->organizationRepository->getAllUsers($project->organization_id);

        return $users;

    }

    public function getByIdWithAllRelations($id) {

        $project = $this->model->with('tasks','tasks.assignedTo')->find($id);

        return $project;

    }

}
