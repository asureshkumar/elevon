<?php

namespace Elevon\repositories;



use Illuminate\Support\Facades\Auth;

abstract class BaseRepository implements EloquentInterface {

    /**
     * @var
     */
    protected $model;

    /**
     * @var array
     */
    protected $input = [];


    /**
     * @param null $params
     * @return mixed
     */
    public function getAll($params = '*')
    {
        return $this->model->all($params);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param $input
     * @return mixed
     */
    public function create($input)
    {
        $input['user_id'] = Auth::user()->id;

        $createModel = $this->model->create($input);

        return $createModel;

    }


    /**
     * @param int $page
     * @param array $columns
     */
    public function paginate($page = 0, $columns = array('*'))
    {

        return $this->model->paginate($page, $columns);

    }

    /**
     * Update the existing model with nre values
     * @param $id
     * @param $input
     * @return mixed
     */
    public function update($id, $input)
    {
        $updatedModel = $this->model->find($id);

        $updatedModel->fill($input);

        $updatedModel->save();

        return $updatedModel;
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $this->model->find($id)->delete();
        
    }

}
