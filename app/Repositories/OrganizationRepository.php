<?php namespace Elevon\Repositories;

use Elevon\Organization;
use Illuminate\Support\Facades\Auth;

class OrganizationRepository extends BaseRepository {

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * OrganizationRepository constructor.
     * @param Organization $model
     * @param UserRepository $userRepository
     */
    public function __construct(Organization $model, UserRepository $userRepository)
  {

      $this->model = $model;

      $this->userRepository = $userRepository;
  }

    public function create($input)
    {
        $organization = parent::create($input);
       // dd($organization->users());

        $organization->users()->attach(Auth::user()->id);

        return $organization;
    }

    public function getAllListForUser() {

        return $this->model->whereUserId(Auth::user()->id)->lists('name','id');

    }

    public function getLatest5OrganizationsByUser() {

    }

    public function getAllUsers($id) {

        $org = $this->model->with('users')->find($id);

        return $org->users;
    }

    public function getNotInOrganizationUsers($id) {

        $users = $this->userRepository->getAll();

        $org = $this->model->with('users')->find($id);

        $currentUsers = collect($org->users->lists('id'));

        $nonUsers = collect([]);

        $users->reduce(function($arr, $user) use ($currentUsers) {
            if($currentUsers->search($user->id) === false) {
                $arr->push($user);
            }
            return $arr;
        },$nonUsers);

        return $nonUsers;
    }

    public function addUser($id, $user_id)
    {
        $organization = $this->model->find($id);
        // dd($organization->users());

        $organization->users()->attach($user_id);

        return $organization;
    }

    public function getUserOrganizations() {

        return $this->model->whereUserId(Auth::user()->id)->get();

    }

    public function getProjects($id) {
        $organization = $this->model->with('projects')->find($id);

        $projects = [];

        if($organization)
            $projects = $organization->projects;

        return $projects;
    }

}
