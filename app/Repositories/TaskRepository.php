<?php namespace Elevon\Repositories;

use Elevon\Task;
use Illuminate\Support\Facades\Mail;

class TaskRepository extends BaseRepository {

    /**
     * @param Task $task
     */
    public function __construct(Task $task) {

        $this->model = $task;

    }

    public function getTasksByProject($id) {

        return $this->model->whereProjectId($id)->with(['assignedTo'])->get();

    }

    public function getTasksByUser($id) {

        return $this->model->whereAssignedTo($id)->with(['project','assignedTo'])->orderBy('project_id')->get();

    }

    public function sendTaskCompletionEmail($id)
    {
        $task = $this->model->with(['project','assignedTo'])->find($id);

        $remainingTasks = $this->model->whereProjectId($task->project->id)->whereUserId($task->assignedTo->id)->where('status', '!=',1)->get();

//        return ['task' => $task, 'remainingTasks' => $remainingTasks];

        Mail::send('emails.tasks.completion', ['remainingTasks' => $remainingTasks, 'task' => $task], function ($m) use ($task,$remainingTasks) {

            $m->from('info@elevonapp.com', 'Elevon App');

            $m->to($task->assignedTo->email, $task->assignedTo->name)->subject('Elevon - Task Completion!');
        });
    }
}