<?php

namespace Elevon;

use Illuminate\Database\Eloquent\Model;


class Organization extends Model
{
    protected $table = "organizations";

    protected $guarded = [];

    public function owner() {

        return $this->belongsTo(User::class,'user_id');

    }

    public function projects() {

        return $this->hasMany(Project::class);

    }

    public function scopeLatest($query) {

        return $query->orderBy('created_at','desc');

    }

    public function users() {

        return $this->belongsToMany(User::class)->withTimestamps();

    }
}
