<?php

namespace Elevon;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = "projects";


    protected $fillable = ['name','description','organization_id','user_id', 'start_date', 'end_date'];

    public function organization() {

        return $this->belongsTo(Organization::class,'organization_id');

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks() {

        return $this->hasMany(Task::class);

    }
}
