<?php

namespace Elevon;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = "tasks";

    protected $fillable = ['name', 'is_milestone', 'completion', 'description', 'user_id', 'project_id', 'assigned_to', 'created_by', 'completion', 'status', 'start_date', 'end_date'];

    private $statusList = [
        "0" => "Not completed",
        "1" => "Completed",
        "2" => "In Progress"
    ];

    public function assignedTo()
    {

        return $this->belongsTo(User::class, 'assigned_to');

    }

    public function createdBy()
    {

        return $this->belongsTo(User::class, 'created_by', 'user_id');

    }

    public function project()
    {

        return $this->belongsTo(Project::class);

    }

    public function getStatusDisplayAttribute()
    {
        return $this->statusList;
    }

    public function getStatusTextAttribute() {

        return $this->statusList[$this->status];
    }

}
