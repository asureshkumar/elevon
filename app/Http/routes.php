<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/', 'HomeController@index');

    Route::group(['middleware' => 'auth'], function() {

        Route::get('/dashboard', 'DashboardController@index');
        Route::get('/home', 'HomeController@index');


        Route::get('/organizations/create', 'OrganizationController@create');
        Route::post('/organizations/create', 'OrganizationController@store');
        Route::get('/organizations', 'OrganizationController@index');
        Route::get('/organizations/{id}', 'OrganizationController@show');
        Route::get('/organizations/{id}/edit', 'OrganizationController@edit');
        Route::post('/organizations/{id}/edit', 'OrganizationController@update');
        Route::post('/organizations/{id}/add-user', 'OrganizationController@addUser');
        Route::delete('/organizations/{id}', ['as' => 'organizations.delete', 'uses' => 'OrganizationController@destroy']);


        Route::get('/projects/create', 'ProjectsController@create');
        Route::post('/projects/create', 'ProjectsController@store');
        Route::get('/projects', 'ProjectsController@index');
        Route::get('/projects/{id}', 'ProjectsController@show');
        Route::post('/projects/{id}/edit', 'ProjectsController@update');
        Route::get('/projects/{id}/edit', 'ProjectsController@edit');
        Route::delete('/projects/{id}', ['as' => 'projects.delete', 'uses' => 'ProjectsController@destroy']);

        Route::get('/myTasks', ['as' => 'tasks.my.list', 'uses' => 'TasksController@myTasks']);
        Route::get('/projects/{id}/tasks',['as' => 'projects.tasks.list', 'uses' => 'TasksController@index']);
        Route::get('/projects/{id}/tasks/create','TasksController@create');
        Route::post('/projects/{id}/tasks/create','TasksController@store');
        Route::get('/projects/{id}/tasks/{taskId}/edit','TasksController@edit');
        Route::post('/projects/{id}/tasks/{taskId}/edit','TasksController@update');
        Route::delete('/projects/{id}/tasks/{taskId}', ['as' => 'tasks.delete', 'uses' => 'TasksController@destroy']);
        Route::delete('/tasks/{id}', ['as' => 'tasks.my.delete', 'uses' => 'TasksController@deleteFromMyTasks']);

    });


});
