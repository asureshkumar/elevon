<?php
/**
 * Created by IntelliJ IDEA.
 * User: ashwinsureshkumar
 * Date: 2016-03-02
 * Time: 6:25 AM
 */

namespace Elevon\Http\Controllers;


use Elevon\Repositories\OrganizationRepository;
use Elevon\Repositories\ProjectRepository;
use Elevon\Repositories\UserRepository;

class DashboardController extends Controller {

    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var ProjectRepository
     */
    private $projectRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @param OrganizationRepository $organizationRepository
     * @param ProjectRepository $projectRepository
     * @param UserRepository $userRepository
     */
    public function __construct(OrganizationRepository $organizationRepository, ProjectRepository $projectRepository, UserRepository $userRepository) {

        $this->organizationRepository = $organizationRepository;

        $this->projectRepository = $projectRepository;

        $this->userRepository = $userRepository;
    }

    public function index() {

        // All Organization
        $organizations = $this->userRepository->getLatestFiveOrganizations();

       // return $organizations;

        // Lastest tasks updates

        // Project simple status

        return view('dashboard.index', compact('organizations'));

    }

}