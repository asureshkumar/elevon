<?php

namespace Elevon\Http\Controllers;

use Elevon\Http\Requests\OrganizationRequest;
use Elevon\Repositories\OrganizationRepository;
use Elevon\User;
use Illuminate\Http\Request;

use Elevon\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
class OrganizationController extends Controller
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;

    public function __construct(OrganizationRepository $organizationRepository) {


        $this->organizationRepository = $organizationRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organizations = $this->organizationRepository->getUserOrganizations();

        $user =  User::with(['organizations','belongingOrganizations'])->find(Auth::user()->id);

        return view('organizations.index', compact('organizations','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('organizations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OrganizationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrganizationRequest $request)
    {
        $model = $this->organizationRepository->create(Input::only('name','description'));

        return Redirect::to('/organizations');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->organizationRepository->getById($id);

        $users = $this->organizationRepository->getAllUsers($id);

        $projects = $this->organizationRepository->getProjects($id);

        return view('organizations.single', compact('model','users','projects'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organization = $this->organizationRepository->getById($id);

        if($organization->user_id != Auth::user()->id)
            return Redirect::back();

        $users = $this->organizationRepository->getAllUsers($id);

        $nonUsers = $this->organizationRepository->getNotInOrganizationUsers($id)->lists('name','id');

        return view('organizations.edit', compact('organization', 'users', 'nonUsers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OrganizationRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrganizationRequest $request, $id)
    {
        $organization = $this->organizationRepository->update($id,Input::only('name','description'));

        $request->session()->flash('success', 'Organization was successfully updated!');

        return Redirect::back();
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function addUser(Request $request, $id){

          $organization = $this->organizationRepository->addUser($id,Input::only('user_id'));

          return Redirect::back();                                                                                      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->organizationRepository->delete($id);

        return redirect('/organizations');
    }
}
