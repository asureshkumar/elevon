<?php

namespace Elevon\Http\Controllers;

use Elevon\Http\Requests\ProjectRequest;
use Elevon\Repositories\OrganizationRepository;
use Elevon\Repositories\ProjectRepository;
use Illuminate\Http\Request;

use Elevon\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class ProjectsController extends Controller
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @param OrganizationRepository $organizationRepository
     * @param ProjectRepository $projectRepository
     */
    public function __construct(OrganizationRepository $organizationRepository, ProjectRepository $projectRepository) {

        $this->organizationRepository = $organizationRepository;

        $this->projectRepository = $projectRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = $this->projectRepository->getAllProjectsForUser();

        return view('projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return Auth::user();
        $organizations = $this->organizationRepository->getAllListForUser();

        return view('projects.create', compact('organizations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProjectRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {
        $input = Input::only(['name', 'description', 'organization_id', 'start_date', 'end_date']);

        $this->projectRepository->create($input);

        return Redirect::to('/projects');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->projectRepository->getByIdWithAllRelations($id);

        $tasks = $model->tasks;

        $model->notCompleted = 0;
        $model->completed = 0;
        $model->inProgress = 0;

        $stats = [];

        foreach($tasks as $task) {
            if($task->status == 0)
                $model->notCompleted++;

            if($task->status == 1)
                $model->completed++;

            if($task->status == 2)
                $model->inProgress++;

//            if(array_key_exists($task->assignedTo->id, $model->userStats)) {
//                $stats[$task->assignedTo]->count = $stats[$task->assignedTo]->count++;
//            } else {
//                $stats[$task->assignedTo->id] = $task->assignedTo;
//                $stats[$task->assignedTo]->count = 1;
//            }
        }

        return view('projects.single', compact('model', 'stats'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = $this->projectRepository->getById($id);

        //return $project;

        return view('projects.edit', compact('project'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = $this->projectRepository->update($id, Input::only('name', 'description', 'start_date', 'end_date'));

        $request->session()->flash('success', 'Project was successfully updated!');

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->projectRepository->delete($id);

        return Redirect::back();
    }
}
