<?php

namespace Elevon\Http\Controllers;

use Elevon\Http\Requests\TaskRequest;
use Elevon\Repositories\ProjectRepository;
use Elevon\Repositories\TaskRepository;
use Exception;
use Illuminate\Http\Request;

use Elevon\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\Debug\Exception\FatalErrorException;

class TasksController extends Controller
{

    /**
     * @var ProjectRepository
     */
    private $projectRepository;
    /**
     * @var TaskRepository
     */
    private $taskRepository;

    /**
     * @param ProjectRepository $projectRepository
     * @param TaskRepository $taskRepository
     */
    public function __construct(ProjectRepository $projectRepository, TaskRepository $taskRepository)
    {

        $this->projectRepository = $projectRepository;

        $this->taskRepository = $taskRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $project = $this->projectRepository->getProjectWithTasks($id);

        return view('tasks.index', compact('project'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $users = $this->projectRepository->getUsersByProjectId($id);

        $users = $users->lists('name', 'id');

        return view('tasks.create', compact('id', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaskRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskRequest $request, $id)
    {
        $input = Input::only(['name', 'description', 'assigned_to', 'start_date', 'end_date']);

        $input['project_id'] = $id;

        $input['status'] = 0;

        $this->taskRepository->create($input);

        return Redirect::to('/projects/' . $id . '/tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $taskId)
    {
        $users = $this->projectRepository->getUsersByProjectId($id);

        $users = $users->lists('name', 'id');

        $task = $this->taskRepository->getById($taskId);

        // return $task;

        return view('tasks.edit', compact('id', 'users', 'task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $taskId)
    {
        $input =  Input::only('name', 'description', 'start_date', 'end_date', 'assigned_to', 'status', 'completion');

        if($input['status'] == 1 && $input['completion'] != 100) {

            return Redirect::back()->withInput()->withErrors(['Invalid' => 'Progress must be 100% before status is changed to "Complete"']);

        } else {

            $task = $this->taskRepository->update($taskId, $input);

            if($input['status'] == 1 && $input['completion'] == 100) {

                $this->taskRepository->sendTaskCompletionEmail($task->id);

                //return view('emails.tasks.completion', $arr);
            }

            $request->session()->flash('success', 'Task was successfully updated!');

            return Redirect::back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $projectId
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($projectId, $id)
    {
        try {
            $this->taskRepository->delete($id);
        } catch (FatalErrorException $e) {

        }

        return redirect()->route('projects.tasks.list', ['projectId' => $projectId]);
    }

    public function myTasks() {

        $tasks = $this->taskRepository->getTasksByUser(Auth::user()->id);

   //     return $tasks;

        return view('tasks.my', compact('tasks'));
    }


    public function deleteFromMyTasks($id){

        try {
            $this->taskRepository->delete($id);
        } catch (FatalErrorException $e) {

        }

        return redirect()->route('tasks.my.list');
    }


}
