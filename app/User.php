<?php

namespace Elevon;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function organizations() {

        return $this->hasMany(Organization::class,'user_id');

    }

    public function belongingOrganizations() {

        return $this->belongsToMany(Organization::class)->withTimestamps();

    }
}
